import json


person_json = '[{"nombre": "Jimena", "cuenta": "jimnna"}, {"nombre": "Natalia", "cuenta": "ntoro"}]'

person_dict = json.loads(person_json)


print("Nombres:")
for person in person_dict:
    print(person["nombre"])

print("\nCuentas:")
for person in person_dict:
    print(person["cuenta"])


num_personas = len(person_dict)
print("\nNúmero de personas que tienen cuenta:", num_personas)