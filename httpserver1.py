
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21080
mihtml = '''
<!DOCTYPE html>
<html>
<body>
<h3 style="color:Tomato;">I am red</h3>

<p style="color:DodgerBlue;">I am blue</p>

<h1>I am big</h1>


<h2>Esta es una foto de mi perro</h2>
<img src="https://mundoanimalsantafe.com.ar/wp-content/uploads/2018/12/cuidado-bulldog-frances-cachorro-1.jpg" alt="Trulli" width="500" height="333">

</body>
</html>'''


class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html; charset=utf-8')
                self.end_headers()
                # El servidor nos manda la respuesta
                self.wfile.write(mihtml.encode("utf-8"))
                return

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor)
        print('Started httpserver on port ' , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()

