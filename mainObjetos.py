class Empleado:
    def __init__(self, nombre, salario, tasa, edad, antigüedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__edad = edad
        self.__antigüedad = antigüedad
    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre,
        tax=self.__impuestos))
        return self.__impuestos
    def ImprimirEdad(self):
        print("Edad de " + self.__nombre + ":" + str(self.__edad))
    def Ahorro(self):
        if self.__antigüedad>1:
            print("Empresa ahorra un 10% en el caso de " + self.__nombre)
        else:
            print("Empresa no ahorra un 10% en el caso de" + self.__nombre)
def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))
emp1 = Empleado("Pepe", 20000, 0.35, 50,2)
emp2 = Empleado("Ana", 30000, 0.30, 40, 0.5)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 20, 4), Empleado("Luisa", 25000, 0.15, 30, 0.3)]
total = 0 
for emp in empleados:
    total += emp.CalculoImpuestos()
    emp.ImprimirEdad()
    emp.Ahorro()
displayCost(total) 