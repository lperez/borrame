class Rectangulo:
    def __init__(self,b=1, a=2):
        self.base = b
        self.altura = a
        self.miperimetro = self.base*2 + self.altura*2
        self.miarea = self.base * self.altura
    def perimetro(self):
        return self.miperimetro
    def area(self):
        return self.miarea
    def __str__ (self):
        return("El rectangulo de base {b}, y altura {a}, tiene un perímetro de {perimetro} y un área de {area}".format(b = self.base, a = self.altura, perimetro = self.miperimetro, area = self.miarea))
rectangulo1 = Rectangulo(2,4)
print(rectangulo1)
