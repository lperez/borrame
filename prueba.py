def calcular_nivel_wbc(tipo_wbc, nivel_minimo, nivel_maximo):
    medida = float(input("Introduzca valor de medida: "))
    if (medida < nivel_minimo):
        print("Nivel de", tipo_wbc, "bajo")
    elif (medida <= nivel_maximo):
        print("Nivel de", tipo_wbc, "normal")
    else:
        print("Nivel de", tipo_wbc, "alto")
tipo_wbc = input("Introduzca tipo de WBC: ")
if (tipo_wbc == "Neutrófilos"):
    calcular_nivel_wbc("Neutrófilos", 1.5, 7.7) 
elif (tipo_wbc == "Linfocitos"):
    calcular_nivel_wbc("Linfocitos", 1.1, 4.5)
elif (tipo_wbc == "Monocitos"):
    calcular_nivel_wbc("Monocitos", 0.1, 0.95)
elif (tipo_wbc == "Eosinófilos"):
    calcular_nivel_wbc("Eosinófilos", 0.02, 0.5)
elif (tipo_wbc == "Basófilos"):
    calcular_nivel_wbc("Basófilos", 0.01, 0.2)
else:
    print("ERROR: tipo de WBC no existente")
