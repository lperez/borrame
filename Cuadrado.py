class Cuadrado:
    """Un ejemplo de una clase para los cuadrados"""
    def __init__(self,l=1):
        self.lado = l
        self.miarea = l**2
    def perimetro(self):
        return self.lado*4
    def area(self):
        return self.miarea
    def __str__(self):
        print("El cuadrdado de lado {l}, tiene un perímetro de {perimetro} y un área de {area}".format(l = self.lado, perimetro = self.perimetro(), area = self.area()))
Cuadrado10 = Cuadrado(10)
Cuadrado20 = Cuadrado(20)

print(Cuadrado20.perimetro())