from http.server import HTTPServer, BaseHTTPRequestHandler

HOST_ADDRESS = "212.128.254.142"
HOST_PORT = 20090

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
        if self.path == "/amigo":
            amigo = [{"nombre": "Jimena", "cuenta": "jimnna"}, {"nombre": "Natalia", "cuenta": "ntoro"}]
            for k in amigo:
                self.wfile.write("<p>Nombre: {}</p>".format(k["nombre"]).encode("utf-8"))
                self.wfile.write("<p>Cuenta: {}</p>".format(k["cuenta"]).encode("utf-8"))

    def do_POST(self):
        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)